# -*- coding: utf-8 -*-
from io import StringIO
import os
import re

from lektor.db import get_alts, Page
from lektor.pluginsystem import Plugin
from lektor.context import get_ctx
from lektor.metaformat import tokenize

from babel.messages import pofile

from lektor_i18n import Translations


def normalize_pofile(file):
    """Normalize PO file date fields by removing alphabetic time zones.

    Some of the POT-Creation-Date timezones are letters but read_po wants numbers, so we
    normalize:
    "POT-Creation-Date: 2018-10-03 22:18+CET\n" -> "POT-Creation-Date: 2018-10-03 22:18\n"
    """
    normalized = StringIO()

    for line in file.readlines():
        if line.startswith('"POT-Creation-Date:') or line.startswith('"PO-Revision-Date:'):
            split_line = line.split('+')
            line = split_line[0] + r'\n"'

        normalized.write(line)

    normalized.seek(0)
    return normalized

class TranslationPercentagePlugin(Plugin):
    name = 'translation percentage'
    description = u'Add your description here.'
    categories = {}
    pofile_regex = re.compile('contents\+(.+?)\.po')

    def on_process_template_context(self, context, **extra):
        def test_function():
            """Calculate how much of the current record is translated."""
            ctx = get_ctx()
            sections = dict(tokenize(ctx.record.contents.as_text().splitlines()))
            pad = get_ctx().pad
            categories = self.categories
            original_page = pad.get('/')
            fields = original_page.datamodel.fields
            alts = [pad.get('/', alt) for alt in get_alts()]
            pct_dict = {}

            for alt in alts:
                total_fields = 0
                translated_fields = 0
                for field in fields:
                    if field.options.get('translate'):
                        total_fields += 1
                        if sections.get(field.name) and self.categories.get(sections[field.name][0]):
                            translated_fields += 1

                pct_dict[alt] = {
                    'total': total_fields,
                    'translated': translated_fields,
                    'percent': translated_fields / total_fields,
                }

            
            return pct_dict
        context['test_function'] = test_function

    def on_before_build_all(self, builder, **extra):
        """Read the gettext catalogs into memory."""
        for filename in os.listdir('i18n'):
            match = self.pofile_regex.fullmatch(filename)
            if not match:
                continue

            locale = match.groups()[0]
            with open('i18n/' + filename, 'r') as f:
                normalized = normalize_pofile(f)
                self.categories[locale] = pofile.read_po(normalized, locale=locale)

    #def on_before_build(self, builder, build_state, source, prog, **extra):
    #    """Before building a page, produce all its alternatives (=translated pages)
    #    using the gettext translations available."""
    #    if isinstance(source, Page):
    #        #breakpoint()
    #        translations.parse_templates('foobar.pot')
    #        source.foobar = "foo and bar!"

